<?php

use InviqaTask\Commands\MonthReport\MonthHandler;

class MonthHandlerTest extends PHPUnit_Framework_TestCase {
    public function testSalaryDateCalculation()
    {
        $assertDate = "28.04.2017";

        $monthHandler = new MonthHandler(new DateTime("01.04.2017"));
        $this->assertEquals($assertDate, $monthHandler->getSalaryDate());
    }

    public function testBonusDateCalculation()
    {
        $assertDate = "19.04.2017";

        $monthHandler = new MonthHandler(new DateTime("01.04.2017"));
        $this->assertEquals($assertDate, $monthHandler->getBonusDate());
    }
}