<?php

class InterpreterTest extends PHPUnit_Framework_TestCase {
    public function testArgumentsDivision()
    {
        $input = ["command", "arg1", "arg2", "arg3"];
        $assertCommand = "command";
        $assertArgs = ["arg1", "arg2", "arg3"];

        $interpreter = \InviqaTask\TerminalInterpreter::fromCommandInput($input);
        $this->assertEquals($assertCommand, $interpreter->getCommand());
        $this->assertEquals($assertArgs, $interpreter->getArgs());
    }

    public function testCommandDefinition()
    {
        $interpreter = \InviqaTask\TerminalInterpreter::fromCommandInput(["build_report", "test.csv"]);
        $command = $interpreter->defineCommand();
        $this->assertEquals("InviqaTask\Commands\BuildReportCommand", get_class($command));
    }
}