<?php
use InviqaTask\Exception\CommandException;
use InviqaTask\Exception\FileException;
use InviqaTask\TerminalInterpreter;

include "bootstrap.php";

array_shift($argv);

try
{
    $interpreter = TerminalInterpreter::fromCommandInput($argv);
    $command = $interpreter->defineCommand();

    $result = $command->runCommand(...$interpreter->getArgs());
    $interpreter->setResult($result);

    $interpreter->response();
}
catch(CommandException $e)
{
    print_r("Error: wrong command. Details: ".$e->getMessage());
}
catch(FileException $e)
{
    print_r("Error: problems with file. Details: ".$e->getMessage());
}
catch(Exception $e)
{
    print_r($e->getMessage());
}
