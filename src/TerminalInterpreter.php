<?php
namespace InviqaTask;


use InviqaTask\Commands\BaseCommand;
use InviqaTask\Commands\CommandResult\Result;
use InviqaTask\Exception\CommandException;
use ReflectionClass;

/**
 * Class TerminalInterpreter
 * @package InviqaTask
 */
class TerminalInterpreter {

    private $command;
    private $args;
    private $result;

    private function __construct() {}

    /**
     * @param $commandParts
     * @return TerminalInterpreter
     */
    public static function fromCommandInput($commandParts)
    {
        $command = $commandParts[0];
        array_shift($commandParts);
        $args = $commandParts;

        $obj = new self();
        $obj->setCommand($command);
        $obj->setArgs($args);

        self::write("Command parsed");
        return $obj;
    }

    /**
     * Function tries to define terminal command from all command classes
     *
     * @return BaseCommand
     * @throws CommandException
     */
    public function defineCommand()
    {
        $definedClasses = get_declared_classes();
        $allCommands = [];

        self::write("Trying to define command");
        foreach($definedClasses as $class)
        {
            $reflection = new ReflectionClass($class);
            if($reflection->implementsInterface("InviqaTask\Commands\BaseCommand"))
                $allCommands[] = $class;
        }

        foreach($allCommands as $commandClass)
        {
            /** @var BaseCommand $obj */
            $obj = new $commandClass();
            if(strcasecmp($this->getCommand(), $obj->getCommandDefinition()) === 0)
            {
                self::write("Command defined");
                $this->checkArguments($commandClass);
                return $obj;
            }
        }

        throw new CommandException("Undefined terminal command!");
    }

    public function response()
    {
        switch($this->getResult()->getStatus())
        {
            case "success":
                print_r("Command successfully executed!");
                break;
            case "error":
                print_r("Errors occurred during command execution! Details: ". $this->getResult()->getMessage());
                break;
        }
    }

    public static function write($message)
    {
        print_r(sprintf("[%s] - %s".PHP_EOL, date("H:i:s"), $message));
    }

    private function checkArguments($commandClass)
    {
        $commandReflection = new ReflectionClass($commandClass);
        $argumentsQuantity = count($commandReflection->getMethod("runCommand")->getParameters());

        if($argumentsQuantity != count($this->getArgs()))
            throw new CommandException("Wrong number of arguments for $commandClass command. You should pass $argumentsQuantity arguments");
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param mixed $command
     */
    private function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @param mixed $args
     */
    private function setArgs($args)
    {
        $this->args = $args;
    }

    /**
     * @return Result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param Result $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
}