<?php

namespace InviqaTask\Commands\CommandResult;

class Result {
    private $status;
    private $message;

    public function getStatus()
    {
        return $this->status;
    }

    public function setSuccess()
    {
        $this->status = "success";
    }

    public function setError()
    {
        $this->status = "error";
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }
}