<?php
/**
 * Created by PhpStorm.
 * User: я
 * Date: 31.01.2017
 * Time: 23:19
 */

namespace InviqaTask\Commands;


use InviqaTask\Commands\CommandResult\Result;

interface BaseCommand {

    /**
     * @param $args
     * @return Result
     */
    public function runCommand($args);
    public function getCommandDefinition();
}