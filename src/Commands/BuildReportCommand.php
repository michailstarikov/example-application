<?php
namespace InviqaTask\Commands;


use DateTime;
use Exception;
use InviqaTask\Commands\CommandResult\Result;
use InviqaTask\Commands\MonthReport\MonthHandler;
use InviqaTask\Exception\FileException;
use InviqaTask\IO\Csv;
use InviqaTask\TerminalInterpreter;

class BuildReportCommand implements BaseCommand {

	/**
	 * @var Csv
	 */
    private $writer;

    /**
     * @param $outputFile
     * @return Result
     */
    public function runCommand($outputFile)
    {
    	$writer = new Csv($outputFile);

        $this->setWriter($writer);
        $this->checkFilePermissions();
        TerminalInterpreter::write("File write permissions checked");

        $result = new Result();
        try
        {
        	$this->writer->createDocument();
	        $this->writer->addRow(["Month name", "Salary payment date", "Bonus payment date"]);

            TerminalInterpreter::write("Starting month processing");
            foreach($this->getMonth() as $month)
            {
                $this->processMonth($month);
            }

            TerminalInterpreter::write("Month successfully processed");
            $result->setSuccess();
        }
        catch(Exception $e)
        {
            $result->setError();
            $result->setMessage($e->getMessage().PHP_EOL.$e->getFile().":".$e->getLine());
        }

        return $result;
    }

    /**
     * @return \Generator
     */
    private function getMonth()
    {
        $currentMonth = new DateTime('first day of this month');
        for($i=0;$i<12;$i++)
        {
            yield $currentMonth;
            $currentMonth = $currentMonth->modify('first day of next month');
        }
    }

    /**
     * @return string
     */
    public function getCommandDefinition()
    {
        return "build_report";
    }

    /**
     * @throws FileException
     */
    public function checkFilePermissions()
    {
        if(file_exists($this->getOutputFile()))
        {
            /**
             * Haven`t got any requirements in case of existing output file. Let say program should rewrite it.
             */
            $file = fopen($this->getOutputFile(), 'w+');
            if(fwrite($file, '') === false)
                throw new FileException("File exists and is not writable!");
        }
        else
        {
            $file = fopen($this->getOutputFile(), 'w+');
            if(fwrite($file, '') === false)
                throw new FileException("File is not writable!");
        }

    }

    /**
     * Function passes month object to business logic handler and then writes result to csv
     *
     * @param DateTime $month
     */
    private function processMonth($month)
    {
        $monthHandler = new MonthHandler($month);
        $this->writer->addRow(
            [$month->format("F"), $monthHandler->getSalaryDate(), $monthHandler->getBonusDate()]
        );
    }

    /**
     * @param mixed $writer
     */
    private function setWriter($writer)
    {
        $this->writer = $writer;
    }
}
