<?php

namespace InviqaTask\Commands\MonthReport;


use DateTime;

class MonthHandler {

    /**
     * @var DateTime
     */
    private $currentMonth;

    function __construct($month)
    {
        $this->setCurrentMonth($month);
    }

    /**
     * @return string
     */
    public function getSalaryDate()
    {
        $currentMonthDay = $this->getCurrentMonth()->modify("last day of this month");
        if(in_array($currentMonthDay->format("D"), ["Sat", "Sun"]))
        {
            do
            {
                $currentMonthDay = $currentMonthDay->sub(new \DateInterval("P1D"));
            }while(in_array($currentMonthDay->format("D"), ["Sat", "Sun"]));
        }

        return $currentMonthDay->format("d.m.Y");
    }

    /**
     * @return string
     */
    public function getBonusDate()
    {
        $currentMonthDay = new DateTime(sprintf("15.%s", $this->getCurrentMonth()->format("m.Y")));
        if(in_array($currentMonthDay->format("D"), ["Sat", "Sun"]))
        {
            do
            {
                $currentMonthDay = $currentMonthDay->add(new \DateInterval("P1D"));
            }while($currentMonthDay->format("D") != "Wed");
        }

        return $currentMonthDay->format("d.m.Y");
    }

    /**
     * @return DateTime
     */
    public function getCurrentMonth()
    {
        return $this->currentMonth;
    }

    /**
     * @param DateTime $currentMonth
     */
    private function setCurrentMonth($currentMonth)
    {
        $this->currentMonth = $currentMonth;
    }
}