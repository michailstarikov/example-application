<?php
namespace InviqaTask\IO;


abstract class DocumentWriterBase
{
	protected $output;

	public function __construct($output) {
		$this->output = $output;
	}

	public abstract function createDocument();
	public abstract function addRow($data);
}