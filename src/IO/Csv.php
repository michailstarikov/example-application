<?php
namespace InviqaTask\IO;


class Csv extends DocumentWriterBase {
    public function createDocument()
    {
        file_put_contents($this->output, '');
    }

    public function addRow($data)
    {
        $resource = fopen($this->output, 'a');
        fputcsv($resource, $data, ";");
        fclose($resource);
    }
}